#include "uniquePointer.h"
#include <iostream>

/**
 * Simple test class that does not really matter
 */
struct TestClass
{
    TestClass() {
        std::cout << "Creating instance of TestClass" << std::endl;
    };

    ~TestClass() {
        std::cout << "Deleting instance of TestClass" << std::endl;
    };

    void
    doSomething() const {
        std::cout << "Doing something" << std::endl;
    };
};

int main() {

// TODO remove comment (after completing implementation of UniquePointer class), run the example and interpret

    typedef UniquePointer<TestClass> PtrType;

    std::cout << "test case 1: deleting in inner context" << std::endl;
    {
        PtrType uptr1(new TestClass());
        {
            PtrType uptr2(uptr1);
            const PtrType& uptr2_cref = uptr2;

            //TODO uncomment and make this work by providing the necessary operators.
            uptr2->doSomething();
            //uptr2_cref->doSomething();
        }
    }

    std::cout << "test case 2: deleting in outer context" << std::endl;
    {
        PtrType uptr3(nullptr);
        {
            PtrType uptr4(new TestClass());
            // TODO forbid the assignment operator and think about how this case can still be made work
            uptr3 = uptr4;
        }
    }

    return 0;
}
