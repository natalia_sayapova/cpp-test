#include <iostream>
#include "linkedList.h"

LinkedListItem::LinkedListItem(int x): value(x), pointer(nullptr) {};

LinkedList::LinkedList(): _first(nullptr), _last(nullptr) {};

LinkedList::LinkedList(const LinkedList &list): _first(nullptr), _last(nullptr) {
    std::cout << "Copy constructor: " << list << std::endl;
    LinkedListItem *current = list.getFirst();

    while (current != nullptr) {
        insert(current -> value);
        current = current -> pointer;
    }
};

LinkedList::~LinkedList() {
    std::cout << "Destructor: " << *this << std::endl;;

    LinkedListItem *current = _first;
    LinkedListItem *temp;

    while (current != nullptr) {
        temp = current -> pointer;

        delete current;
        current = temp;
    }
}

/**
   Create element with value x and insert it in the last of the list.
*/
void LinkedList::insert(int x) {
    LinkedListItem *newItem = new LinkedListItem(x);

    if (_first == nullptr) {
        // if list is empty we should change first element
        _first = newItem;
    } else {
        // else we are just appending new element to the last element.
        _last -> pointer = newItem;
    }

    _last = newItem;
};

/**
   Remove first element with the value x
*/
void LinkedList::remove(int x) {
    LinkedListItem *previous = _first;
    LinkedListItem *toRemove = _first -> pointer;

    // Search for element, which should be removed
    while (toRemove -> value != x) {
        previous    = toRemove;
        toRemove = toRemove -> pointer;
    }

    // Previous element is pointing on the next element now.
    // (Was: ... previous -> to remove -> next ...)
    previous -> pointer = toRemove -> pointer;

    delete toRemove;
};

/**
   Return element with specified value.
*/
LinkedListItem *LinkedList::search(int x) {
    LinkedListItem *current = _first;

    while (current -> value != x && current != nullptr) {
        current = current -> pointer;
    }

    return current;
};

/**
   Connect specified list to the last element.
*/
void LinkedList::connect(LinkedList &list) {
    LinkedListItem *current = list.getFirst();

    while (current != nullptr) {
        insert(current -> value);
        current = current -> pointer;
    }
};

std::ostream &operator<<(std::ostream &strm, const LinkedList &list) {
    LinkedListItem *current = list.getFirst();

    while (current != nullptr) {
        strm << current -> value << " ";
        current = current -> pointer;
    }

    return strm;
};

int main() {
    LinkedList list1;
    LinkedList list2;

    list1.insert(3);
    list1.insert(5);
    list1.insert(6);
    list1.insert(4);
    std::cout << list1 << std::endl;

    list1.remove(6);

    LinkedListItem *item = list1.search(4);
    std::cout << item -> value << std::endl;

    list2.insert(13);
    list2.insert(21);
    list2.insert(-6);
    std::cout << list2 << std::endl;

    list1.connect(list2);
    std::cout << list1 << std::endl;

    list2.remove(-6);
    std::cout << list1 << std::endl;

    return 0;
}
