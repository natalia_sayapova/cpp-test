#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H 
#include <vector>

class LinkedListItem {
public:
    int value;
    LinkedListItem *pointer;
    
    LinkedListItem(int x);
};

class LinkedList {
private:
    LinkedListItem *_first;
    LinkedListItem *_last;

public:
    friend std::ostream &operator<<(std::ostream &strm, const LinkedList &list);
        
    LinkedList();
    LinkedList(LinkedList const &list);
    ~LinkedList();

    void insert(int x);
    void remove(int x);
    LinkedListItem *search(int x);
    void connect(LinkedList &list);
    LinkedListItem *getFirst() const {
        return _first;
    };
    LinkedListItem *getLast() const {
        return _last;
    };
};

#endif
