#ifndef _UNIQUE_POINTER_H
#define _UNIQUE_POINTER_H

template <class T>

class UniquePointer {
private:
    T* _pointer;

public:
    UniquePointer<T>(T*);
    UniquePointer<T>(UniquePointer<T>&);
    //UniquePointer(const UniquePointer<T>&) = delete;
    UniquePointer<T>& operator=(UniquePointer<T>&);// = default;
    
    ~UniquePointer<T>();

    void reset(T*);
    T* release();

    T* operator->() const;
    
    T& operator*(); 
};

#endif
