#ifndef __BINARY_TREE_H_
#define __BINARY_TREE_H_

class BinaryTreeNode {
public:
    int value;
    BinaryTreeNode *parent;
    BinaryTreeNode *left;
    BinaryTreeNode *right;

    BinaryTreeNode(int);
};

class BinaryTree {
private:
    BinaryTreeNode *_root;

public:
    BinaryTree();

    void insert(int x);
    void remove(int x);
    void printLevel(std::vector<BinaryTreeNode*> items) const;
    void print() const;
    
    BinaryTreeNode* getParent(int) const;
    BinaryTreeNode* getSuccessor(int) const;
    BinaryTreeNode* getPredeccessor() const;
    BinaryTreeNode* search(int) const;
    BinaryTreeNode* subTreeMin(BinaryTreeNode*) const;
    BinaryTreeNode* subTreeMax(BinaryTreeNode*) const;
    BinaryTreeNode* min() const;

    BinaryTreeNode* getRoot() const {
        return _root;
    }
};

#endif
