#include <iostream>
#include <vector>
#include "binaryTree.h"

BinaryTreeNode::BinaryTreeNode(int x):
    value(x),
    parent(nullptr),
    left(nullptr),
    right(nullptr)
{};

BinaryTree::BinaryTree(): _root(nullptr) {};

void BinaryTree::insert(int x) {
    BinaryTreeNode *newItem = new BinaryTreeNode(x);

    if (_root == nullptr) {
        _root = newItem;
    } else {
        BinaryTreeNode *current = _root;
        BinaryTreeNode *parent = nullptr;

        while (current != nullptr) {
            parent = current;

            // search for the parent
            if (current -> value > x) {
                current = current -> left;
            } else {
                current = current -> right;
            }
        }

        newItem -> parent = parent;

        // Insert in the right spot
        if (x < parent -> value) {
            parent -> left = newItem;
        } else {
            parent -> right = newItem;
        }
    }
}

void BinaryTree::remove(int x) {
    // Element which will be replaced
    BinaryTreeNode *toReplace = search(x);
    // Element which will be removed
    BinaryTreeNode *toRemove = nullptr;
    // Element for replacing
    BinaryTreeNode *replacing = nullptr;

    if (toReplace -> left == nullptr || toReplace -> right == nullptr) {
        toRemove = toReplace;
    } else {
        toRemove = getSuccessor(x);
    }

    if (toRemove -> left != nullptr) {
        replacing = toRemove -> left;
    } else {
        replacing = toRemove -> right;
    }

    if (replacing != nullptr) {
        replacing -> parent = toRemove -> parent;
    }

    if (toRemove -> parent == nullptr) {
        _root = replacing;
    } else {
        if (toRemove == (toRemove -> parent) -> left) {
            (toRemove -> parent) -> left = replacing;
        } else {
            (toRemove -> parent) -> right = replacing; 
        }
    }

    if (toRemove -> value != x) {
        toReplace -> value = toRemove -> value;
    }
}

BinaryTreeNode* BinaryTree::subTreeMax(BinaryTreeNode *subTreeRoot) const {
    BinaryTreeNode *current = subTreeRoot; 

    while (current != nullptr) {
        current = current -> right;
    }

    return current;
}

BinaryTreeNode* BinaryTree::subTreeMin(BinaryTreeNode *subTreeRoot) const {
    BinaryTreeNode *current = subTreeRoot; 

    while (current -> left != nullptr) {
        current = current -> left;
    }

    return current;
};

BinaryTreeNode* BinaryTree::min() const {
    return subTreeMin(_root);
}

BinaryTreeNode* BinaryTree::search(int x) const {
    BinaryTreeNode *current = _root;

    while (current -> value != x && current != nullptr) {
        if (current -> value > x) {
            current = current -> left;
        } else {
            current = current -> right;
        }
    }

    return current;
}

BinaryTreeNode* BinaryTree::getSuccessor(int x) const {
    BinaryTreeNode *current = search(x);

    if (current -> right != nullptr) {
        return subTreeMin(current -> right);
    }

    BinaryTreeNode *ancestor = current -> parent;

    while (ancestor != nullptr && ancestor -> value < x) {
        ancestor = ancestor -> parent;
    };

    return ancestor;
}

BinaryTreeNode* BinaryTree::getPredeccessor(int x) const {
    BinaryTreeNode *current = search(x);
    
    if (current -> left != nullptr) {
        return subTreeMax(current -> left);
    }

    return nullptr;
}

void BinaryTree::printLevel(std::vector<BinaryTreeNode*> items) const {
    BinaryTreeNode *item;
    std::vector<BinaryTreeNode*> nextLevel;
    bool hasNextLevel = false;
    static int k = 0;

    for (int i = 0; i < items.size(); i++) {
        item = items[i];
        if (item != nullptr) {
            std::cout << "  " << item -> value << "  ";
            auto it = nextLevel.end();
            nextLevel.insert(it, item -> left);
            it = nextLevel.end();
            nextLevel.insert(it, item -> right);

            if (item -> left != nullptr || item -> right != nullptr) {
                hasNextLevel = true;
            }
        } else {
            std::cout << "   ";
            auto it = nextLevel.end();
            nextLevel.insert(it, nullptr);
            it = nextLevel.end();
            nextLevel.insert(it, nullptr);
        }
    }

    std::cout << std::endl;

    if (hasNextLevel) {
        printLevel(nextLevel);
    }
};

void BinaryTree::print() const{
    std::vector<BinaryTreeNode*> firstLevel;
    firstLevel.insert(firstLevel.begin(), getRoot());

    printLevel(firstLevel);
}

int main() {
    BinaryTree tree;

    tree.insert(5);
    tree.insert(2);
    tree.insert(3);
    tree.insert(1);
    tree.insert(7);
    tree.insert(8);
    tree.insert(6);
    tree.print();

    tree.remove(7);

    tree.print();

    return 0;
}
