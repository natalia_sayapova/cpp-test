#include <iostream>
#include "uniquePointer.h"

template <class T>
UniquePointer<T>::UniquePointer(T* x): _pointer(x) {
    std::cout << "pointer created" << std::endl;
}

template <class T>
UniquePointer<T>::UniquePointer(UniquePointer<T>& anotherPointer) {
    std::cout << "pointer copy constructor" << std::endl;
    _pointer = anotherPointer.release();
}

template <class T>
UniquePointer<T>::~UniquePointer<T>() {
    if (_pointer != nullptr) {
        std::cout << "pointer destructor" << std::endl;
        delete _pointer;
    }
}

template <class T>
T* UniquePointer<T>::release() {
    std::cout << "release" << std::endl;
    T* value = _pointer;
    _pointer = nullptr;

    return value;
}

template <class T>
void UniquePointer<T>::reset(T* x) {
    if (_pointer != nullptr) {
        delete _pointer;
    }

    _pointer = x;
}

template <class T>
T* UniquePointer<T>::operator->() const {
    std::cout << "->" << std::endl;
    return _pointer;
}

template <class T>
T& UniquePointer<T>::operator*() {
    return *_pointer;
}

template <class T>
UniquePointer<T>& UniquePointer<T>::operator=(UniquePointer<T>& anotherPointer) {
    std::cout << "=" << std::endl;
     _pointer = anotherPointer.release();
     return *this;
}

/**
 * Simple test class that does not really matter
 */
struct TestClass
{
    int property;

    TestClass(int x): property(x) {
        std::cout << "Creating instance of TestClass" << std::endl;
    };

    ~TestClass() {
        std::cout << "Deleting instance of TestClass" << std::endl;
    };

    void
    doSomething() const {
        std::cout << "Doing something" << std::endl;
    };
};

int main() {

// TODO remove comment (after completing implementation of UniquePointer class), run the example and interpret

    typedef UniquePointer<TestClass> PtrType;

    std::cout << "test case 1: deleting in inner context" << std::endl;
    {
        PtrType uptr1(new TestClass(5));
        {
            PtrType uptr2(uptr1);
            const PtrType& uptr2_cref = uptr2;

            //TODO uncomment and make this work by providing the necessary operators.
            uptr2->doSomething();
            uptr2_cref->doSomething();
        }
    }

    std::cout << std::endl << std::endl;

    std::cout << "test case 2: deleting in outer context" << std::endl;
    {
        PtrType uptr3(nullptr);
        {
            PtrType uptr4(new TestClass(8));
            // TODO forbid the assignment operator and think about how this case can still be made work
            uptr3 = uptr4;
            uptr3->a;
            //uptr4->a;
        }
    }

    return 0;
}
